#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

int numbers[] = {
    13, -5, 64, 42, -2, 0, 23, 37, 32,
};

/* simple char array for demo purposes */
char *words[] = {
    "really",
    "just",
    "actually",
    "quite",
};

/* show: print each entry in array */
void showints(int array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        printf("%d\n", array[i]);
    }
}

/* show: print each entry in array */
void showwords(char *array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        puts(array[i]);
    }
}

/* scmp: string compare of *p1 and *p2 */
int scmp(const void *p1, const void *p2) {
    char *v1, *v2;
    v1 = *(char **) p1;
    v2 = *(char **) p2;
    return strcmp(v1, v2);
}

/* icmp: integer compare of *p1 and *p2 */
int icmp(const void *p1, const void *p2) {
    int v1, v2;
    v1 = *(int *) p1;
    v2 = *(int *) p2;
    if (v1 < v2) {
        return -1;
    } else if (v1 == v2) {
        return 0;
    } else {
        return 1;
    }
    /*
    Note: avoid "return v1-v2"
    because if v1 is a large negative and v2 a large positive value
    the overflow would produce an incorrect answer
    */
}

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
};

/* HTML characters, e.g. &AElig; is 1igature of A and E. */
/* Values are Unicode/IS010646 encoding. */
/* just for demo purposses, NOT exhaustive, must be in order */
Nameval htmlchars[] = {
    "&AElig;", 0x00C6,
    "&HilbertSpace;", 0x210B,
    "&Integral;", 0x222B,
    "&Lambda;", 0x039B,
    "&OElig;", 0x0152,
    "&Pi;", 0x03A0,
    "&copy;", 0x00A9,
    "&diamondsuit;", 0x2666,
    "&frac12;", 0x00BD,
    "&lat;", 0x2AAB,
    "&psi;", 0x03C8,
    "&yen", 0x00A5,
    "&zeta;", 0x03B6,
};

/* nvcmp: compare two Nameval names */
int nvcmp(const void *va, const void *vb) {
    const Nameval *a, *b;
    a = (Nameval *) va;
    b = (Nameval *) vb;
    return strcmp(a->name, b->name);
}

/* lookupbsearch: use bsearch to find name in tab, return index */
int lookupbsearch(char *name, Nameval tab[], int ntab) {
    Nameval key, *np;
    key.name = name;
    key.value = 0; /* unused, anything will do */
    np = (Nameval *) bsearch(&key, tab, ntab, sizeof(tab[0]), nvcmp);
    if (np == NULL) {
        return -1;
    } else {
        return np-tab;
    }
}

int main(int argc, char **argv) {
    puts("unsorted words:");
    showwords(words, NELEMS(words));
    
    qsort(words, NELEMS(words), sizeof(words[0]), scmp);

    puts("sorted words:");
    showwords(words, NELEMS(words));


    puts("unsorted numbers:");
    showints(numbers, NELEMS(numbers));
    
    qsort(numbers, NELEMS(numbers), sizeof(numbers[0]), icmp);

    puts("sorted numbers:");
    showints(numbers, NELEMS(numbers));

    int foundat;
    char *searchword;
    
    searchword = "&Lambda;";
    foundat = lookupbsearch(searchword, htmlchars, NELEMS(htmlchars));
    printf("%s found at index %d\n", searchword, foundat);

    searchword = "nope!";
    foundat = lookupbsearch(searchword, htmlchars, NELEMS(htmlchars));
    printf("%s found at index %d\n", searchword, foundat);
}
