#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

int numbers[] = {
    13, -5, 64, 42, -2, 0, 23, 37, 32,
};

/* show: print each entry in array */
void show(int array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        printf("%d\n", array[i]);
    }
}

/* swap: interchange v[i] and v[j] */
void swap(int v[], int i, int j) {
    int temp;
    temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

/* quicksort: sort v[0]..v[n-1] into increasing order */
void quicksort(int v[], int n) {
    int i, last;
    if (n <= 1) { /* nothing to do */
        return;
    }
    swap(v, 0, rand() % n); /* move pivot elem to v[0] */
    last = 0;
    for (i = 1; i < n; i++) { /* partition */
        if (v[i] < v[0]) {
            swap(v, ++last, i);
        }
    }
    swap(v, 0, last); /* restore pivot */
    quicksort(v, last); /* recursively sort each part */
    quicksort(v+last+1, n-last-1);
}

int main(int argc, char **argv) {
    puts("unsorted:");
    show(numbers, NELEMS(numbers));

    quicksort(numbers, NELEMS(numbers));

    puts("sorted:");
    show(numbers, NELEMS(numbers));
}
