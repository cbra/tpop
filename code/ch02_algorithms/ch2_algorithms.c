#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

/* scmp: string compare of *p1 and *p2 */
int scmp(const void *p1, const void *p2) {
    char *v1, *v2;
    v1 = *(char **) p1;
    v2 = *(char **) p2;
    return strcmp(v1, v2);
}

/* lookup: sequential search for word in array */
int lookupSeq(char *word, char *array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        if (strcmp(word, array[i]) == 0) {
            return i;
        }
    }
    return -1;
}

/* show: print each entry in array */
void show(char *array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        puts(array[i]);
    }
}

int main(int argc, char **argv) {
    char *flab[] = {
        "actually",
        "just",
        "quite",
        "really",
        "cowbell"
    };

    puts("unsorted:");
    show(flab, NELEMS(flab));

    qsort(flab, NELEMS(flab), sizeof(flab[0]), scmp);

    puts("\nsorted:");
    show(flab, NELEMS(flab));

    /* note: size is elements*64bit (pointers) */
    printf("size: %lu, elements: %lu\n", sizeof(flab), NELEMS(flab));

    printf("found: %d\n", lookupSeq("bonkers", flab, NELEMS(flab)));
}
