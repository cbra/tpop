#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o ex_2_15 ex_2_15.c eprintf.o
*/

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
    Nameval *next; /* in chain */
};

enum {
    NHASH = 97, /* hash buckets, makes sense to use a prime number */
    MULTIPLIER = 31 /* string hash algorithm multiplier, should be a small prime, 31 and 37 are empirically good values */
};

Nameval *symtab[NHASH]; /* a symbol table */

/* hash: compute hash value of string */
unsigned int hash(char *str) {
    unsigned int h;
    unsigned char *p;
    h = 0;
    for (p = (unsigned char *) str; *p != '\0'; p++) {
        h = MULTIPLIER * h + *p;
    }
    return h % NHASH;
}

/* lookup: find name in symtab, with optional create */
/* optional create to avoid double hash computing */
Nameval *lookup(char *name, int create, int value) {
    int h;
    Nameval *sym;
    h = hash(name);
    for (sym = symtab[h]; sym != NULL; sym = sym->next) {
        if (strcmp(name, sym->name) == 0) {
            return sym;
        }
    }
    if (create) {
        /* create new entry and add it to the front of this bucket's list */
        sym = (Nameval *) emalloc(sizeof(Nameval));
        sym->name = name; /*assumed allocated elsewhere */
        sym->value = value;
        sym->next = symtab[h];
        symtab[h] = sym;
    }
    return sym;
}

/* printnv: print name and value using format in arg, suitable for apply functions */
void printnv(Nameval *p, void *arg) {
    char *fmt;
    fmt = (char *) arg;
    printf(fmt, p->name, p->value);
}

/* inccounter: increment counter *arg, suitable for apply functions */
void inccounter(Nameval *p, void *arg) {
    int *ip;
    /* p is unused */
    ip = (int *) arg;
    (*ip)++;
}

/* applyinorder: inorder application of fn to treep */
void apply(Nameval *hashtable[], void (*fn)(Nameval*, void*), void *arg) {
    int bucket;
    Nameval * e;
    for (bucket = 0; bucket < NHASH; bucket++) {
        for (e = hashtable[bucket]; e != NULL; e = e->next) {
            (*fn)(e, arg);
        }
    }
}

int main(int argc, char **argv) {
    Nameval *nv;
    lookup("&frac12;", 1, 0x00BD);
    lookup("&Lambda;", 1, 0x039B);
    lookup("&psi;", 1, 0x03C8);
    lookup("&OElig;", 1, 0x0152);
    lookup("&AElig;", 1, 0x00C6);

    int i = 0;
    apply(symtab, inccounter, &i);
    printf("%d elements in symtab:\n", i);
    apply(symtab, printnv, "%s: %x\n");
}
