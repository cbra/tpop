#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o ex_2_17 ex_2_17.c eprintf.o
*/

enum {
    NHASH = 23, /* hash buckets, makes sense to use a prime number */
    MULTIPLIER = 31 /* hash algorithm multiplier, should be a small prime, 31 and 37 are empirically good values (for character strings...) */
};

struct point2d {
	int x;
	int y;
};

struct fpoint2d {
	float x;
	float y;
};

struct polarpoint {
	float rho;
	float theta;
};

struct point3d {
	int x;
	int y;
	int z;
};

unsigned int hashpoint2d(struct point2d *p) {
    unsigned int h = 0;
    h = MULTIPLIER * h + p->x;
    h = MULTIPLIER * h + p->y;
    return h % NHASH;
}

unsigned int hashpointfloat2d(struct fpoint2d *p) {
    unsigned int h = 0;
    h = MULTIPLIER * h + p->x;
    h = MULTIPLIER * h + p->y;
    return h % NHASH;
}

// identical to hashpointfloat2d
unsigned int hashpolarpoint(struct polarpoint *p) {
    unsigned int h = 0;
    h = MULTIPLIER * h + p->rho;
    h = MULTIPLIER * h + p->theta;
    return h % NHASH;
}

unsigned int hashpoint3d(struct point3d *p) {
    unsigned int h = 0;
    h = MULTIPLIER * h + p->x;
    h = MULTIPLIER * h + p->y;
    h = MULTIPLIER * h + p->z;
    return h % NHASH;
}

void zero(int a[], int n) {
    int i;
    for(i = 0; i < n; i++) {
        a[i] = 0;
    }
}

int main(int argc, char **argv) {
    int x, y;
    int hashtable[NHASH];
    // note: default is *not* all elements 0, it's whatever at this block of memory has been before
    zero(hashtable, NHASH);
    int max = 7;
    for (x = -max; x <= max; x++) {
        for (y = -max; y <= max; y++) {
            struct point2d p;
            p.x = x;
            p.y = y;
            hashtable[hashpoint2d(&p)]++;
            // printf("x: %d, y: %d, hashed: %u\n", x, y, hashpoint2d(&p));
        }
    }
    int i;
    for (i = 0; i < NHASH; i++) {
        printf("%d ", hashtable[i]);
    }
    puts("");
    // TODO: check this for the other points as well
}
