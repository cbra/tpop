#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o 2_7_lists 2_7_lists.c eprintf.o
*/

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
    Nameval *next; /* in list */
};

/*newitem: create a new Nameval item from name and value */
Nameval *newitem(char *name, int value) {
    Nameval *newp;
    newp = (Nameval *) emalloc(sizeof(Nameval));
    newp->name = name;
    newp->value = value;
    newp->next = NULL;
    return newp;
}

/* addfront: add newp to front of listp */
Nameval *addfront(Nameval *listp, Nameval *newp) {
    newp->next = listp;
    return newp;
}

/* addend: add newp to end of listp */
Nameval *addend(Nameval *listp, Nameval *newp) {
    Nameval *p;
    if (listp == NULL) {
        return newp;
    }
    for (p = listp; p->next != NULL; p = p->next) {}
    p->next = newp;
    return listp;
}

/* lookup: sequential search for name in listp */
Nameval *lookup(Nameval *listp, char *name) {
    for( ;listp != NULL; listp = listp->next) {
        if (strcmp(name, listp->name) == 0) {
            return listp;
        }
    }
    return NULL;
}

/* apply: execute fn with parameter arg for each element of listp */
void apply(Nameval *listp, void (*fn)(Nameval*, void*), void *arg) {
    for( ;listp != NULL; listp = listp->next) {
        (*fn)(listp, arg); /* call the function with arg */
    }
}

/* printnv: print name and value using format in arg, suitable for apply */
void printnv(Nameval *p, void *arg) {
    char *fmt;
    fmt = (char *) arg;
    printf(fmt, p->name, p->value);
}

/* inccounter: increment counter *arg, suitable for apply */
void inccounter(Nameval *p, void *arg) {
    int *ip;
    /* p is unused */
    ip = (int *) arg;
    (*ip)++;
}

/* freeall: free all elements of listp. */
/* Note this is not suitable for apply, it must save next pointers in a local variable befor the current can be freed */
void freeall(Nameval *listp) {
    Nameval *next;
    for( ; listp != NULL; listp = next) {
        next = listp->next;
        /* assumes name is freed elsewhere */
        free(listp);
    }
}

/* delitem: delete first name from listp */
Nameval *delitem(Nameval *listp, char *name) {
    Nameval *p, *prev;
    prev = NULL;
    for (p = listp; p != NULL; p = p->next) {
        if (strcmp(name, p->name) == 0) {
            if (prev == NULL) {
                listp = p->next;
            } else {
                prev->next = p->next;
            }
            free(p); /* assumes name is freed elsewhere */
            return listp;
        }
        prev = p;
    }
    eprintf("delitem: %s not in list", name);
    return NULL; /* can't get here */
}

int main(int argc, char **argv) {
    // Nameval *ucaelig = newitem("&AElig;", 0x00C6);

    Nameval *nvlist = NULL; /* must be initialized with NULL, just Nameval *nvlist; would segfault in addend */

    int n = 0;
    apply(nvlist, inccounter, &n);
    printf("%d elements in nvlist\n", n);

    nvlist = addfront(nvlist, newitem("&Lambda;", 0x039B));
    nvlist = addfront(nvlist, newitem("&frac12;", 0x00BD));

    n = 0;
    apply(nvlist, inccounter, &n);
    printf("%d elements in nvlist\n", n);
    apply(nvlist, printnv, "%s: %x\n");

    nvlist = addend(nvlist, newitem("&psi;", 0x03C8));
    nvlist = addend(nvlist, newitem("&OElig;", 0x0152));

    n = 0;
    apply(nvlist, inccounter, &n);
    printf("%d elements in nvlist\n", n);
    apply(nvlist, printnv, "%s: %x\n");

    Nameval *found = lookup(nvlist, "&psi;");
    printf("found: %s, %X\n", found->name, found->value);

    nvlist = delitem(nvlist, "&frac12;");
    n = 0;
    apply(nvlist, inccounter, &n);
    printf("%d elements in nvlist\n", n);
    apply(nvlist, printnv, "%s: %x\n");

    // nvlist = delitem(nvlist, "nope!"); // this would exit the program!

    freeall(nvlist);
    // nvlist can now be reused, it's NULL
}
