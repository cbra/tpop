#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o 2_9_hashtables 2_9_hashtables.c eprintf.o
*/

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
    Nameval *next; /* in chain */
};

enum {
    NHASH = 97, /* hash buckets, makes sense to use a prime number */
    MULTIPLIER = 31 /* string hash algorithm multiplier, should be a small prime, 31 and 37 are empirically good values */
};

Nameval *symtab[NHASH]; /* a symbol table */

/* hash: compute hash value of string */
unsigned int hash(char *str) {
    unsigned int h;
    unsigned char *p;
    h = 0;
    for (p = (unsigned char *) str; *p != '\0'; p++) {
        h = MULTIPLIER * h + *p;
    }
    return h % NHASH;
}

/* lookup: find name in symtab, with optional create */
/* optional create to avoid double hash computing */
Nameval *lookup(char *name, int create, int value) {
    int h;
    Nameval *sym;
    h = hash(name);
    for (sym = symtab[h]; sym != NULL; sym = sym->next) {
        if (strcmp(name, sym->name) == 0) {
            return sym;
        }
    }
    if (create) {
        /* create new entry and add it to the front of this bucket's list */
        sym = (Nameval *) emalloc(sizeof(Nameval));
        sym->name = name; /*assumed allocated elsewhere */
        sym->value = value;
        sym->next = symtab[h];
        symtab[h] = sym;
    }
    return sym;
}

int main(int argc, char **argv) {
    Nameval *nv;
    char *str;

    str = "&frac12;";
    nv = lookup(str, 1, 0x00BD);
    printf("found/inserted: %s: %X\n", nv->name, nv->value);

    str = "&Lambda;";
    nv = lookup(str, 1, 0x039B);
    printf("found/inserted: %s: %X\n", nv->name, nv->value);

    str = "&psi;";
    nv = lookup(str, 0, 0x039B);
    if (nv != NULL) {
        // in this case *sym in lookup was by default created as NULL?
        // cf. 2_7_lists.c where *nameval had to be explicitly assigned NULL, isn't this the same case here?
        printf("found/inserted: %s: %X\n", nv->name, nv->value);
    }

    lookup("&psi;", 1, 0x03C8);
    lookup("&OElig;", 1, 0x0152);
    lookup("&AElig;", 1, 0x00C6);

    str = "&psi;";
    nv = lookup(str, 0, 0x039B);
    printf("found/inserted: %s: %X\n", nv->name, nv->value);

    str = "&OElig;";
    nv = lookup(str, 1, 0x1234); /* Note: this implementation won't replace, only lookup/insert */
    printf("found/inserted: %s: %X\n", nv->name, nv->value);
}
