#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

/* simple char array for demo purposes */
char *flab[] = {
    "actually",
    "just",
    "quite",
    "really",
    NULL
};

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
};

/* HTML characters, e.g. &AElig; is 1igature of A and E. */
/* Values are Unicode/IS010646 encoding. */
/* just for demo purposses, NOT exhaustive */
Nameval htmlchars[] = {
    "&AElig;", 0x00C6,
    "&Aacute;", 0x00C1,
    "&Acirc;", 0x00C2,
    "&Agrave;", 0x00C0,
    "&Alpha;", 0x0391,
    "&And;", 0x2A53,
    "&Auml;", 0x00C4,
    "&Beta;", 0x0392,
    "&Chi;", 0x03A7,
    "&Delta;", 0x0394,
    "&Epsilon;", 0x0395,
    "&Fouriertrf;", 0x2131,
    "&GreaterTilde;", 0x2273,
    "&HilbertSpace;", 0x210B,
    "&Integral;", 0x222B,
    "&Iota;", 0x0399,
    "&Kappa;", 0x039A,
    "&Lambda;", 0x039B,
    "&Mu;", 0x039C,
    "&Not;", 0x2AEC,
    "&OElig;", 0x0152,
    "&Pi;", 0x03A0,
    "&QUOT;", 0x0022,
    "&Rcaron;", 0x0158,
    "&Sigma;", 0x03A3,
    "&TripleDot;", 0x20DB,
    "&Ucirc;", 0x00DB,
    "&VerticalBar;", 0x2223,
    "&Wedge;", 0x22C0,
    "&Xi;", 0x039E,
    "&Yuml;", 0x0178,
    "&Zeta;", 0x0396,
    "&aacute;", 0x00E1,
    "&bcy;", 0x0431,
    "&copy;", 0x00A9,
    "&diamondsuit;", 0x2666,
    "&frac12;", 0x00BD,
    "&gt;", 0x003E,
    "&infintie;", 0x29DD,
    "&lat;", 0x2AAB,
    "&nexists;", 0x2204,
    "&omega;", 0x03C9,
    "&psi;", 0x03C8,
    "&star;", 0x2606,
    "&vzigzag;", 0x299A,
    "&yen", 0x00A5,
    "&zeta;", 0x03B6,
};

/* lookupSeq: sequential search for word in array. array is expected to be NULL terminated. */
int lookupSeq(char *word, char *array[]) {
    int i;
    for (i = 0; array[i] != NULL; i++) {
        if (strcmp(word, array[i]) == 0) {
            return i;
        }
    }
    return -1;
}

/* lookupBin: binary search for name in tab, return index */
int lookupBin(char *name, Nameval tab[], int ntab) {
    int low, high, mid, cmp;
    low = 0;
    high = ntab - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        cmp = strcmp(name, tab[mid].name);
        if (cmp < 0) {
            high = mid - 1;
        } else if (cmp > 0) {
            low = mid + 1;
        } else { /* found match */
            return mid;
        }
    }
    return -1; /* no match */
}

/* show: print each entry in array */
void show(char *array[], size_t sz) {
    int i;
    for (i = 0; i < sz; i++) {
        puts(array[i]);
    }
}

int main(int argc, char **argv) {
    /* note: size is elements*64bit (pointers) */
    printf("size: %lu, elements: %lu\n", sizeof(flab), NELEMS(flab));
    printf("found: %d\n", lookupSeq("quite", flab));

    printf("The HTML table has %lu words\n", NELEMS(htmlchars));

    int foundat;
    char *searchword;
    
    searchword = "&frac12;";
    foundat = lookupBin(searchword, htmlchars, NELEMS(htmlchars));
    printf("%s found at index %d\n", searchword, foundat);

    searchword = "nope!";
    foundat = lookupBin(searchword, htmlchars, NELEMS(htmlchars));
    printf("%s found at index %d\n", searchword, foundat);
}
