#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o 2_6_growing_arrays 2_6_growing_arrays.c eprintf.o
*/

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
};

struct NVtab {
    int nval; /* current number of values */
    int max; /* allocated number of values */
    Nameval *nameval; /* array of name-value pairs */
} nvtab;

enum {
    NVINIT = 1,
    NVGROW = 2
};

/* addname: add new name and value to nvtab, return index of added value or -1 if an error occured */
int addname(Nameval newname) {
    Nameval *nvp;
    if (nvtab.nameval == NULL) { /* first time */
        nvtab.nameval = (Nameval *) malloc(NVINIT * sizeof(Nameval));
        if (nvtab.nameval == NULL) {
            return -1;
        }
        nvtab.max = NVINIT;
        nvtab.nval = 0; /* initialized with 0 so that the final return always is correct */
    } else if (nvtab.nval >= nvtab.max) { /* grow */
        /* realloc to a temporary variable to make sure not to lose the original pointer in case of error */
        nvp = (Nameval *) realloc(nvtab.nameval, (NVGROW*nvtab.max) * sizeof(Nameval));
        if (nvp == NULL) {
            return -1;
        }
        nvtab.max *= NVGROW;
        nvtab.nameval = nvp;
    }
    nvtab.nameval[nvtab.nval] = newname;
    return nvtab.nval++;
}

/*newitem: create a new Nameval item from name and value */
Nameval *newitem(char *name, int value) {
    Nameval *newp;
    newp = (Nameval *) emalloc(sizeof(Nameval));
    newp->name = name;
    newp->value = value;
    return newp;
}

/* delname: remove first matching Nameval from nvtab, return 1 if an element was removed, otherwise 0 */
int delname(char *name) {
    int i;
    for (i = 0; i < nvtab.nval; i++) {
        if (strcmp(nvtab.nameval[i].name, name) == 0) {
            /* Note: memmove also works in dest and src overlap, memcpy doesn't */
            memmove(nvtab.nameval+i, nvtab.nameval+i+1, (nvtab.nval-(i+1)) * sizeof(Nameval));
            nvtab.nval--;
            return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv) {

    printf("nvtab current: %d, max: %d\n", nvtab.nval, nvtab.max);
    Nameval *uclambda = newitem("&Lambda;", 0x039B);
    Nameval *frac12 = newitem("&frac12;", 0x00BD);
    Nameval *psi = newitem("&psi;", 0x03C8);
    Nameval *ucoelig = newitem("&OElig;", 0x0152);
    Nameval *ucaelig = newitem("&AElig;", 0x00C6);

    int idx;
    idx = addname(*uclambda);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);
    idx = addname(*frac12);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);
    idx = addname(*psi);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);
    idx = addname(*ucoelig);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);
    idx = addname(*ucaelig);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);

    int res;
    res = delname("&frac12;");
    printf("deleted? %d, nvtab current: %d, max: %d\n", res, nvtab.nval, nvtab.max);
    res = delname("nope!");
    printf("deleted? %d, nvtab current: %d, max: %d\n", res, nvtab.nval, nvtab.max);
    res = delname("&OElig;");
    printf("deleted? %d, nvtab current: %d, max: %d\n", res, nvtab.nval, nvtab.max);

    idx = addname(*frac12);
    printf("added at: %d, nvtab current: %d, max: %d\n", idx, nvtab.nval, nvtab.max);

}
