#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eprintf.h"

/*
- compile library to object:
$ clang -o eprintf.o -c eprintf.c
- then compile program and statically include the lib:
$ clang -o 2_8_trees 2_8_trees.c eprintf.o
*/

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int  value;
    Nameval *left; /* lesser in binary tree */
    Nameval *right; /* bigger in binary tree */
};

/*newitem: create a new Nameval item from name and value */
Nameval *newitem(char *name, int value) {
    Nameval *newp;
    newp = (Nameval *) emalloc(sizeof(Nameval));
    newp->name = name;
    newp->value = value;
    newp->left = NULL;
    newp->right = NULL;
    return newp;
}

/* insert: insert newp in treep, return treep */
Nameval *insert(Nameval *treep, Nameval *newp) {
    int cmp;
    if (treep == NULL) {
        return newp;
    }
    cmp = strcmp(newp->name, treep->name);
    if (cmp == 0) {
        weprintf("insert: duplicate entry %s ignored", newp->name);
    } else if (cmp < 0) {
        treep->left = insert(treep->left, newp);
    } else {
        treep->right = insert(treep->right, newp);
    }
    return treep;
}

/* lookup: look up name in tree treep */
Nameval *lookup(Nameval *treep, char *name) {
    int cmp;
    if (treep == NULL) {
        return NULL;
    }
    cmp = strcmp(name, treep->name);
    if (cmp == 0) {
        return treep;
    } else if (cmp < 0) {
        return lookup(treep->left, name);
    } else {
        return lookup(treep->right, name);
    }
}

/* nrlookup: non-recursively look up name in tree treep */
Nameval *nrlookup(Nameval *treep, char *name) {
    int cmp;
    while (treep != NULL) {
        cmp = strcmp(name, treep->name);
        if (cmp == 0) {
            return treep;
        } else if (cmp < 0) {
            treep = treep->left;
        } else {
            treep = treep->right;
        }
    }
    return NULL;
}

/* printnv: print name and value using format in arg, suitable for apply functions */
void printnv(Nameval *p, void *arg) {
    char *fmt;
    fmt = (char *) arg;
    printf(fmt, p->name, p->value);
}

/* inccounter: increment counter *arg, suitable for apply functions */
void inccounter(Nameval *p, void *arg) {
    int *ip;
    /* p is unused */
    ip = (int *) arg;
    (*ip)++;
}

/* applyinorder: inorder application of fn to treep */
void applyinorder(Nameval *treep, void (*fn)(Nameval*, void*), void *arg) {
    if (treep == NULL) {
        return;
    }
    applyinorder(treep->left, fn, arg);
    (*fn)(treep, arg);
    applyinorder(treep->right, fn, arg);
}

/* applypostorder: postorder application of fn to treep */
void applypostorder(Nameval *treep, void (*fn)(Nameval*, void*), void *arg) {
    if (treep == NULL) {
        return;
    }
    applypostorder(treep->left, fn, arg);
    applypostorder(treep->right, fn, arg);
    (*fn)(treep, arg);
}

int main(int argc, char **argv) {
    Nameval *nvtree = NULL; /* must be initialized with NULL, just Nameval *nvlist; would segfault in insert */

    int n = 0;
    applyinorder(nvtree, inccounter, &n);
    printf("%d elements in nvtree\n", n);

    nvtree = insert(nvtree, newitem("&frac12;", 0x00BD));
    nvtree = insert(nvtree, newitem("&Lambda;", 0x039B));
    nvtree = insert(nvtree, newitem("&psi;", 0x03C8));
    nvtree = insert(nvtree, newitem("&OElig;", 0x0152));
    nvtree = insert(nvtree, newitem("&AElig;", 0x00C6));

    nvtree = insert(nvtree, newitem("&OElig;", 0x0152));

    n = 0;
    applyinorder(nvtree, inccounter, &n);
    printf("%d elements in nvtree\n", n);

    puts("in order:");
    applyinorder(nvtree, printnv, "%s: %x\n");

    puts("post order:");
    applypostorder(nvtree, printnv, "%s: %x\n");
}
