#include <stdio.h>

int sum(int a[], int sz);

int main(int argc, char **argv) {
   int a[] = {1, 2, 3};
   printf("the sum is %d\n", sum(a, 3));
   printf("the sum except the first is %d\n", sum(a + 1, 2));
}

int sum(int a[], int sz) {
    int v = 0;
    int i;
    for (i = 0; i < sz; i++) {
        v += a[i];
    }
    return v;
}

