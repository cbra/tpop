"use strict";
var fs = require("fs");
var nonword = "\n";
var maxgen = 10000;
var choice = function (arr) { return arr.length ? arr[Math.floor(Math.random() * arr.length)] : undefined; };
var Model = /** @class */ (function () {
    function Model() {
        this.internal = new Map();
    }
    Model.prototype.makeKey = function (prefix) {
        return prefix.w1 + " " + prefix.w2;
    };
    Model.prototype.add = function (prefix, suffix) {
        var key = this.makeKey(prefix);
        var sufs = this.internal.get(key);
        if (sufs == undefined) {
            sufs = [suffix];
        }
        else {
            sufs.push(suffix);
        }
        this.internal.set(key, sufs);
    };
    Model.prototype.randomSuffix = function (prefix) {
        var key = this.makeKey(prefix);
        var sufs = this.internal.get(key);
        if (sufs != undefined) {
            return choice(sufs);
        }
        return undefined; //--noImplicitReturns
    };
    Model.prototype.inspect = function () {
        // console.log(`model size: ${this.internal.size}`)
        this.internal.forEach(function (v, k, m) {
            console.log("".concat(v.length, "\t").concat(k, "\t").concat(v.join(" ")));
        });
    };
    return Model;
}());
var model = new Model();
var data = fs.readFileSync(0, "utf-8"); // read from stdin (fd 0)
var pattern = new RegExp('\\s+');
var words = data.split(pattern);
var w1 = nonword;
var w2 = nonword;
words.forEach(function (word, i, array) {
    model.add({ w1: w1, w2: w2 }, word);
    w1 = w2;
    w2 = word;
});
model.add({ w1: w1, w2: w2 }, nonword);
w1 = nonword;
w2 = nonword;
for (var i = 0; i < maxgen; i++) {
    var w = model.randomSuffix({ w1: w1, w2: w2 });
    if (w == nonword || w == undefined) {
        // undefined case shouldn't happen when the model is iterated it this
        // way, but may happen when someone just uses the API with some
        // arbitrary words that have not been in the input,
        // e.g. model.randomSuffix({"foo", "bar"})
        break;
    }
    console.log(w);
    w1 = w2;
    w2 = w;
}
