const fs = require("fs");
const nonword = "\n";
const maxgen = 10000

// could just be a tuple [string, string]?
type Prefix = {
    w1 : string
    w2 : string
}

const choice = (arr: any[]) => arr.length ? arr[Math.floor(Math.random() * arr.length)] : undefined

class Model {
    // ES6 introduced a Map type.
    // Problem: arbitrary types don't work as map keys,
    // only strings and numbers do.
    internal: Map<string, string[]>

    constructor() {
        this.internal = new Map<string, string[]>()
    }

    private makeKey(prefix: Prefix) {
        return prefix.w1 + " " + prefix.w2
    }

    add(prefix: Prefix, suffix: string) {
        let key = this.makeKey(prefix)
        let sufs = this.internal.get(key)
        if (sufs == undefined) {
            sufs = [suffix]
        } else {
            sufs.push(suffix)
        }
        this.internal.set(key, sufs)
    }

    randomSuffix(prefix: Prefix): string|undefined {
        let key = this.makeKey(prefix)
        let sufs = this.internal.get(key)
        if (sufs != undefined) {
            return choice(sufs)
        }
        return undefined  //--noImplicitReturns
    }

    inspect() {
        // console.log(`model size: ${this.internal.size}`)
        this.internal.forEach((v, k, m) => {
            console.log(`${v.length}\t${k}\t${v.join(" ")}`)
        })
    }
}

let model: Model = new Model()

let data: string = fs.readFileSync(0, "utf-8") // read from stdin (fd 0)

const pattern = new RegExp('\\s+')
let words = data.split(pattern)

let w1 = nonword
let w2 = nonword

words.forEach((word, i, array) => {
    model.add({w1, w2}, word)
    w1 = w2
    w2 = word
})

model.add({w1, w2}, nonword)

w1 = nonword
w2 = nonword

for (let i = 0; i < maxgen; i++) {
    let w = model.randomSuffix({w1, w2})
    if (w == nonword || w == undefined) {
        // undefined case shouldn't happen when the model is iterated it this
        // way, but may happen when someone just uses the API with some
        // arbitrary words that have not been in the input,
        // e.g. model.randomSuffix({"foo", "bar"})
        break;
    }
    console.log(w)
    w1 = w2
    w2 = w
}
