
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Markov {

    private final static int MAXGEN = 10000;
    private final static String NONWORD = "\n";
    private final static int NPREF = 2;
    private Map<String, List<String>> model;
    // Note: String[] as key doesn't work, probably because of how it's hashed.
    // When I tried to copy-create a new array in add(), resembling the example
    // code, I even got an NPE in generate() -- overwriting hash() is required!
    private String[] prefix;
    private Random r;

    public Markov() {
        model = new HashMap<>();
        prefix = new String[NPREF];
        Arrays.fill(prefix, NONWORD);
        r = new Random();
    }

    private void advancePrefix(String s) {
        if (prefix.length - 1 >= 0) {
            System.arraycopy(prefix, 1, prefix, 0, prefix.length - 1);
        }
        prefix[prefix.length - 1] = s;
    }

    private String makeKey() {
        return String.join(" ", prefix);
    }

    public void add(String suffix) {
        String key = makeKey();
        List<String> sufs = model.computeIfAbsent(key, k -> new ArrayList<>());
        sufs.add(suffix);
        advancePrefix(suffix);
    }

    public void generate() {
        Arrays.fill(prefix, NONWORD);
        for (int i = 0; i < MAXGEN; i++) {
            List<String> sufs = model.get(makeKey());
            String w = sufs.get(r.nextInt(sufs.size()));
            if (w.equals(NONWORD)) {
                break;
            }
            System.out.println(w);
            advancePrefix(w);
        }
    }

    public static void main(String[] args) throws IOException {
        Markov markov = new Markov();

        // Slower, and for some reason doesn't remove all whitespaces
//        Pattern p = Pattern.compile("\\s+");
//        Scanner sc = new Scanner(System.in);
//        sc.useDelimiter(p);
//        String token;
//        while (sc.hasNext()) {
//            token = sc.next();
//            if (token.trim().length() > 0) {
//                markov.add(token);
//            }
//        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine()) != null) {
            for (String w : line.split(" ")) {
                if (w.length() > 0) {
                    markov.add(w);
                }
            }
        }
        markov.add(NONWORD);
        markov.generate();
    }

}
