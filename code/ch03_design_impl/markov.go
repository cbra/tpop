package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

const (
	MAXGEN = 10000
	NONWORD = "\n"
)

type Prefix struct {
	w1, w2 string
}

func main() {
	rand.Seed(time.Now().UnixMilli())
	w1, w2 := NONWORD, NONWORD
	model := make(map[Prefix][]string)
	
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		w := scanner.Text()
		p := Prefix{w1, w2}
		model[p] = append(model[p], w)  // this works without ever creating a new []string?
		w1, w2 = w2, w
	}
	p := Prefix{w1, w2}
	model[p] = append(model[p], NONWORD)

	//for k, v := range model {
	//	fmt.Printf("%d\t%s\t%s\n", len(v), k.w1 + " " + k.w2, strings.Join(v, " "))
	//}

	w1, w2 = NONWORD, NONWORD

	for i := 0; i < MAXGEN; i++ {
		sufs := model[Prefix{w1, w2}]
		w := sufs[rand.Intn(len(sufs))]
		if w == NONWORD {
			break
		}
		fmt.Println(w)
		w1, w2 = w2, w
	}
}
