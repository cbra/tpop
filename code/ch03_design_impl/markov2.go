package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

const (
	MAXGEN = 10000
	NONWORD = "\n"
)

type Prefix struct {
	w1, w2 string
}

func (p *Prefix) advance(word string) {
	p.w1 = p.w2
	p.w2 = word
}

type Model map[Prefix][]string

type Chain struct {
	Model
	Prefix
}

func NewChain() Chain {
	return Chain{
		Model:  make(Model),
		Prefix: Prefix{NONWORD, NONWORD},
	}
}

func (c *Chain) build(input *os.File) {
	scanner := bufio.NewScanner(input)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		w := scanner.Text()
		c.Model[c.Prefix] = append(c.Model[c.Prefix], w)  // this works without ever creating a new []string?
		c.Prefix.advance(w)  // re-using a modified Prefix instance as new map key also works, without defining how it's hashed and compared?
	}
	c.Model[c.Prefix] = append(c.Model[c.Prefix], NONWORD)
}

func (c *Chain) generate(n uint) {
	rand.Seed(time.Now().UnixMilli())
	c.Prefix = Prefix{NONWORD, NONWORD}
	for i := uint(0); i < n; i++ {
		sufs := c.Model[c.Prefix]
		w := sufs[rand.Intn(len(sufs))]
		if w == NONWORD {
			break
		}
		fmt.Println(w)
		c.Prefix.advance(w)
	}
}

func (c *Chain) inspectModel() {
	for k, v := range c.Model {
		fmt.Printf("%d\t%s\t%s\n", len(v), k.w1 + " " + k.w2, strings.Join(v, " "))
	}
}

func main() {
	c := NewChain()
	c.build(os.Stdin)
	//c.inspectModel()
	c.generate(MAXGEN)
}
