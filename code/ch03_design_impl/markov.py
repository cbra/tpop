#!/usr/bin/env python3
# coding: utf-8

import fileinput
import sys
from collections import defaultdict
from random import choice

maxgen = 10000
nonword = '\n'

w1, w2 = nonword, nonword
model = defaultdict(list)

for line in fileinput.input():
    for word in line.split():
        model[(w1, w2)].append(word)
        w1, w2 = w2, word

# runtime improvement - may be a memory issue for huge input,
# but the model is probably worse
# for word in sys.stdin.read().split():
#     model[(w1, w2)].append(word)
#     w1, w2 = w2, word

model[(w1, w2)].append(nonword)

w1, w2 = nonword, nonword

for _ in range(maxgen):
    w = choice(model[(w1, w2)])
    if w == nonword:
        break
    print(w)
    w1, w2 = w2, w
