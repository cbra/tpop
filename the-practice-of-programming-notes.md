# The Practice of Programming - Brian W. Kernighan, Rob Pike (1999)

- available compilers

```
$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/9/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none:hsa
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 9.4.0-1ubuntu1~20.04.1' --with-bugurl=file:///usr/share/doc/gcc-9/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++,gm2 --prefix=/usr --with-gcc-major-version-only --program-suffix=-9 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib=auto --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none=/build/gcc-9-Av3uEd/gcc-9-9.4.0/debian/tmp-nvptx/usr,hsa --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 9.4.0 (Ubuntu 9.4.0-1ubuntu1~20.04.1) 

$ clang -v
clang version 10.0.0-4ubuntu1 
Target: x86_64-pc-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
Found candidate GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/9
Found candidate GCC installation: /usr/lib/gcc/x86_64-linux-gnu/9
Selected GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/9
Candidate multilib: .;@m64
Selected multilib: .;@m64
```

- note: clang by default outputs warnings, gcc should always be used with -Wall

## in general about the book

- old C style, pre-C99
- no "{...}" on blocks (just on single-line blocks, that's the authors style)
- in some code fragments undeclared identifiers are used `n = 1;` instead of `int n = 1;`,
  very likely intends that the variable was declared before (but not good style in an
  educational text)
- no exercise solutions :/

### Ch 1 - Style

- in the first chapter most paragraphs start with a nice concise summary sentence, these could be collected,
  unfortunately the authors don't keep that through the rest of the book 

#### 1.1 Names

- Use descriptive names for globals, short names for locals
- Be consistent
- Use active names for functions
- Be accurate

##### Ex 1-1

- natuarally I'd expect TRUE is 1 and FALSE is 0 (e.g. `while (1)` is an infinite loop)
  - also: int function returns usually use 0 for "all ok" and != 0 for "something wrong", plays together nicely with `if (retVal)`
- `not_eof = false` is a double negative, why not `eof = TRUE`?

##### Ex 1-2

- would return 1 for identical strings which is likely not intended, so `strcmp(s, t) < 0` or rename the function to `smallerOrEqual` (a name is a comment, code is probably correct)
- also add documentation

##### Ex 1-3

read this code out loud

        if ((falloc(SMRHSHSCRTCH, S_FEXT|0644, MAXRODDHSH)) < 0)
            ...
            
* people have to be able to talk about it!
* when reading, we read it in our head and that should not be hard

* fun fact: C linkers were just obligated to look at the first 6 characters of a name... until C89

#### 1.2 Expressions and Statements

- Indent to show structure
- Use the natural form for expressions (e.g. avoid unnecessary negation in conditions)
- Parenthesize to resolve ambiguity
- Break up complex expressions
- Be clear
- Be careful with side effects

* cf. "Sequence Point"

##### Ex 1-4

improve

        if ( !(c == 'y' || c == 'Y') )
            return;
        // improved:
        if ( c != 'y' && c != 'Y' ) {
            return;
        }


        length = (length < BUFSIZE) ? length : BUFSIZE;
        // improved:
        if (length > BUFSIZE) {
            length = BUFSIZE;
        }
        // doesn't need to be >= because when equal, why change!


        flag = flag ? 0 : 1;
        // improved:
        flag = !flag;


        quote = (*line == '"') ? 1 : 0;
        // improved:
        is_quote = line[0] == '"'


        if (val & 1)
            bit = 1;
        else
            bit = 0;
        // improved:
        bit = val & 1;


##### Ex 1-5

What's wrong with this excerpt?
    
        int read(int *ip) {
            scanf("%d", ip);
            return *ip;
        }
        ...
        insert($graph[vert], read(&val), read(&ch));

- "In C and C++, the order of execution of side effects is undefined"
  so which read value goes into val and ch (?)
* "Sequence Points"


##### Ex 1-6

List all the different outputs this could produce with various orders of evaluation:

        int n = 1;
        printf("%d %d\n", n++, n++);

Try it on as many compilers as you can, to see what happens in practice.

- gcc and clang:

        # gcc ex01_06.c -> ex01_06_gcc
        ex01_06.c: In function ‘main’:
        ex01_06.c:5:28: warning: operation on ‘n’ may be undefined [-Wsequence-point]
            5 |    printf("%d %d\n", n++, n++);
              |                           ~^~

        # clang ex01_06.c -> ex01_06_clang
        ex01_06.c:5:23: warning: multiple unsequenced modifications to 'n' [-Wunsequenced]
           printf("%d %d\n", n++, n++);
                              ^    ~~
        $ ./ex01_06_gcc
        2 1
        
        $ ./ex01_06_clang
        1 2


#### 1.3 Consistency and Idioms

- Use a consistent indentation and brace style
  - Pick one style, _preferably ours_, use it consistently, and don't waste time arguing. ;)
  - if you work on a program you didn't write, preserve the style you find there
- Use idioms for consistency
  - A central part of learning any language is developing a familiarity with its idioms.
    * would be nice to always have a definitive source to look up the idioms
    * there are some languages that are more idiomatic (Python, Go (?) -- e.g. google "writing idiomatic ...")
      and some are less (Perl)
- Use else-ifs for multi-way decisions

* Loop style: Rob Pike comes from a point of "human reasoning", Joe from "formal reasoning"

##### Ex 1-7

Rewrite these C/C++ excerpts more clearly

        // assume the issue is just the indentation, not the missing code in the first 3 cases
        if (istty(stdin)) {...};
        else if (istty(stdout)) {...};
        else if (istty(stderr)) {...};
        else return(0);
        // or
        if ( !(istty(stdin)) && !(istty(stdout)) && !(istty(stderr)) ) {
            return;
        }
        

        return retval;
        
        
        for (k = 0; k < 5; k++) {
            double dx;
            scanf("%lf", &dx);
            x += dx;
        }

##### Ex 1-8

Identify the errors in this Java fragment and repair it by rewriting with an idiomatic loop:

        // wrong:
        int count = 0;
        while (count < total) {
            count++;
            if (this.getName(count) == nametable.userName()) {
                return(true);
            }
        }
        
        // fixed (? assumed this.getName(0) shouldn't be skipped)
        for (int count = 0; count < total; count++) {
            if (this.getName(count).equals(nametable.userName())) {
                return true;
            }
        }


#### 1.4 Function Macros

- quite a C-specific problem
- "Avoid function macros" - 'nuff said
- Macros work by textual substitution
- Parenthesize the macro body and arguments

* Go seems to have figured the constants as macro replacements out, Go constants
  don't need types and work like a replacement

##### Ex 1-9

- not completely parenthesized, everything must be parentesized, and each c must be!
- c evaluated multiple times
        
        // avoid multiple evaluations, works if 0 to 9 is consecutive in the charachterset
        #define ISDIGIT(c) ((unsigned)((c) -'0') <= 9


#### 1.5 Magic Numbers

- Give names to magic numbers
- Define numbers as constants, not macros. In C and C++, integer constants can be defined with an enum statement:

        enum {
            MINROW = 1, /* top edge */
            MINCOL = 1, /* left edge */
            ...
        };
        
  - C++ also supports the keyword `const`, Java has `final`
  - C also has const values but they cannot be used as array bounds, so the enum statement remains the method of choice in C.

- Use the language to calculate the size of an object
  - e.g. `sizeof(array[0])`
  - array length for C/C++: `#define NELEMS(array) (sizeof(array)/sizeof(array[0]))`

* 0 example: Go has it worse with nil, there's "interface-wrapped" nil which isn't actually nil!


##### Ex 1-10

- How would you rewrite these definitions to minimize potential errors?
  - as enum/const and with documentation (what does the Z mean?)

* the Z is a 2!
* if we have not both values, just one and 1/...
* conversion functions would be better


#### 1.6 Comments

- Don't belabor the obvious
- Comment functions and global data.
  (functions, global variables, constant definitions, fields in structures and classes, and anything else where a brief summary can aid understanding)
- Don't comment bad code, rewrite it
- Don't contradict the code
  - pg. 26, first "fixed" code, is there a semicolon missing or is the empty line actually enough to delimit the last line as _not_ loop body?
    the physical book has a single semicolon in the empty line!
- Clarify, don't confuse

##### Ex 1-11

Comment on these comments

- the comment describes a "contains" check, the function definition says "insert", also comment below code line (in ths style) is pretty uncommon
- comment is both, missing a part of the check, and unnecessary, doesn't tell why
- function comment should be "Write a message with line counts", comments inside the function are useless


#### 1.7 Why Bother?

- well-written code is easier to read and to understand, almost surely has fewer errors [...]
  sloppy code is bad code -- not just awkward and hard to read, but often broken

- also compare to Python's `import this`
  - "readability" is a term that occurs several times
  - I'd add "simplicity"

Personal thoughts
* understanding of context and language idioms often required to read code
* understanding of context requirement should be reduced as much as possible
* laguage idioms should be easy to look up (sadly few introductory texts include a focus to idiomatic code)
* sometimes the knowledge is just necessary (realtime/segmenter),
  but mostly it should be possible to write readable and understandable code
* back at university we made fun of writing complicated code, e.g. to keep it short or for performance reasons
  (e.g. code golf), but that's a very bad habit (can be un-learned once one has to maintain one's own old code)
  (or there are things like "the underhanded C contest")


### Ch 2 - Algorithms and Datastructures

#### 2.1 Searching

#### 2.2 Sorting

#### 2.3 Libraries

- pg 37
  "Nevertheless, it's a good idea to use bsearch instead of writing your own.
  Over the years, binary search has proven surprisingly hard for programmers to get right."
  so is Quicksort, btw.


##### Ex 2-1 Iterative Quicksort

Quicksort is most naturally expressed recursively. Write it iteratively and compare the two versions.
(Hoare describes how hard it was to work out quicksort iteratively, and how neatly it fell into place when he did it recursively.)

#### 2.4 A Java Quicksort

- Java didn't have generics back in 1999?


##### Ex 2-2 

Our Java quicksort does a fair amount of type conversion as items are cast from their original type (like Integer) to Object
and back again. Experiment with a version of Quicksort.sort that uses the specific type being sorted, to estimate what
performance penalty is incurred by type conversions.

#### 2.5 O-Notation

##### Exercise 2-3
What are some input sequences that might cause a quicksort implementation to display worst-case behavior?
Try to find some that provoke your library version into running slowly.
Automate the process so that you can specify and perform a large number of experiments easily.

##### Exercise 2-4
Design and implement an algorithm that will sort an array of n integers as slowly as possible.
You have to play fair: the algorithm must make progress and eventually terminate, and the implementation
must not cheat with tricks like time-wasting loops. What is the complexity of your algorithm as a function of n?

#### 2.6 Growing Arrays

##### Exercise 2-5
In the code above, del name doesn't call realloc to return the memory freed by the deletion.
Is this worthwhile? How would you decide whether to do so?

##### Exercise 2-6
Implement the necessary changes to addname and del name to delete items by marking deleted items as unused.
How isolated is the rest of the program from this change?

#### 2.7 Lists

- "Notice that freeall does not free 1istp->name. It assumes that the name field of each Nameval will be freed somewhere else, or was never allocated."
  So whats the lifecycle of these names? The examples are stack variables because they are in code? But what if the values are input from e.g. a file?

- delitem: why exit when item not in list and not just return the unmodified list? because there's no signal if something was deleted or not?

- also, isn't printing from subroutines bad style? (also happens in 2.8)

##### Exercise 2-7
Implement some of the other list operators: copy, merge, split, insert before or after a specific item.
How do the two insertion operations differ in difficulty? How much can you use the routines we've written,
and how much must you create yourself?

- copy: shallow or deep?
- merge: meaning attach one list to another? they are unsorted, don't see a need for actual "merging".
- split: meaning split after or before a specific element? what would be the use cases in an unsorted and duplicates-allowing list?
- insert before requires to keep track of the previous element, to change it's "next" pointer
- insert after is more trivial, let the new elements "next" point to the current elements "next", then the current elements "next" to the new element

- deep copy could reuse "apply" and "addend" (pass the new as argument to the apply "fn", addend a copy of the current element)
  but that would be inefficient, better to use a straightforward iteration that doesn' iterate the new list each time
- can't see how to re-use routines for the other new functions?


##### Exercise 2-8
Write recursive and iterative versions of reverse, which reverses a list. Do not create new list items: re-use the existing ones.
- ex_2_8.c

##### Exercise 2-9
Write a generic List type for C. The easiest way is to have each list item hold a voids, that points to the data.
Do the same for C++ by defining a template and for Java by defining a class that holds lists of type Object.
What are the strengths and weaknesses of the various languages for this job?

- too much work to also dust of my C++, and in Java dealing with raw Objects is very outdated

##### Exercise 2-10
Devise and implement a set of tests for verifying that the list routines you write are correct.
Chapter 6 discusses strategies for testing.

- too much work

#### 2.8 Trees

- trees need balancing, this is mentioned at some point in the chapter, but imo not stressed enough,
  such a simple tree implementation would cause more harm than being useful

##### Exercise 2-11
Compare the performance of lookup and nrlookup. How expensive is recursion compared to iteration?

- I'd assume the recursive version gets more expensive the deeper the tree is, because function calls
  add stack frames, at a certain depth there may even be a stack overflow.
  Imo all functional languages use tail recursion elimination to optimize this.

##### Exercise 2-12
Use in-order traversal to create a sort routine. What time complexity does it have? Under what conditions
might it behave poorly? How does its performance compare to our quicksort and a library version?

- I don't understand the question, in-order traversing a binary tree traversed in sort order...
  does it mean "build a tree from some unsorted collection to then process it in order"?
- Under the assumption that quicksort or a library sort can be applied to the original collection,
  that would most likely be more performant, I'd guess library > textbook quicksort > textbook tree sort.
  

##### Exercise 2-13
Devise and implement a set of tests for verifying that the tree routines are correct.

- too much work

#### 2.9 Hash Tables

- hash tables today likely with re-hashing (not mentioned, but asked in ex 2-16)
- what about sorted lists/arrays for buckets?
- all functions before were passed (and mostly returned) the "container" (list, tree, ...)
  but the examples here use a different style, lookup doesn't reference symtab in it's signature,
  it uses the global variable...
- Note: the hashtable shown isn't able to update a value, only lookup/insert (but could be extended without much effort?)

##### Exercise 2-14
Our hash function is an excellent general-purpose hash for strings. Nonetheless, peculiar data might
cause poor behavior. Construct a data set that causes our hash function to perform badly.
Is it easier to find a bad set for different values of NHASH?

- first part: no time
- if NHASH == MULTIPLIER or a multiple of it, it should be easier to find collissions

##### Exercise 2-15
Write a function to access the successive elements of the hash table in unsorted order.

- ex_2_15.c (apply)

##### Exercise 2-16
Change lookup so that if the average list length becomes more than x, the array is grown automatically
by a factor of y and the hash table is rebuilt.

- that's rehashing, too much work

Notes:
- in Go hashes are pass by reference (unusually) so that rehashing is supported (restructuring the actual elements)
- open hashing closed addressing (chaining) vs. closed hashing open addressing (no chaining, probing)
- Go uses chaining of 4-element-arrays but may switch to open addressing (performance, less memory usage)
- Joe: open adressing should be used more often, but instead chaining got to be the de-facto standard...
- Go's map internals, Talk mentioning reasons why they didn't do open adressing: https://youtu.be/Tl7mi9QmLns?t=1370
  

##### Exercise 2-17
Design a hash function for storing the coordinates of points in 2 dimensions. How easily does your
function adapt to changes in the type of the coordinates, for example from integer to floating point
or from Cartesian to polar coordinates, or to changes from 2 to higher dimensions?

- ex_2_17.c

Notes:
- for polar coordinates my solution wouldn't work because they are not unique - i.e. should normalize the coordinates before hashing
- there are specific hash functions for 2d points with nice properties
  - cantor pairing function (high density packing for small integers, so guaranteed different buckets for "small" coordinates)
    or other pairing function (hilbert curve, space filling curve)

#### 2.10 Summary

- the Supertrace idea was used e.g. in fuzzing tools

- how relevant is this today?
  - feels more like a refresher than getting any best practises, good advice or new things to learn
  - algorithms and their properties are always important to know, but here the discussion is quite shallow and
    basically every language has default sort and search implementations (although I forgot that C has function
    pointers and also already provides natural implementations)
  - there are also datastructure/container implementations in every language
  - the authors even mention this, use (standard) library features, or "borrow" a tested and true implementation,
    only for very specific cases it must be done manually, but then the shown very basic techniques won't help much
- pointer-style: nice for C, but impractical or even impossible for most other languages
  - there are several good arguments against "pointer based data structures", re performance and
    computer/memory architecture, with pagin, cache misses and so on it's mostly better to use datastructures that
    have a sequential memory layout (i.e. arrays)
  - I assume that common implementations of more complex data structures like trees also use unterlying arrays,
    but I didn't check (would need a lot of bookkeeping and is a time-memory tradeoff).
- I don't like recursion in imperative languages, it's hard to read and get right with pointers and state modification
  all over the place, and almost always less performant than iterative approaches.


### Ch 3 - Design and Implementation

#### 3.1 The Markov Chain Algorithm


#### 3.2 Data Structure Alternatives

> The prefix and the set of all its possible suffixes we'll call a state, which is standard terminology for Markov algorithms.

- isn't the prefix a state and the suffixes are state-changing operators, or options or whatever?

> What happens if a phrase appears more than once? [...] (putting it multiple times in the suffix list or count appearences) We've tried it with and without counters; without is easier, since adding a suffix doesn't require checking whether it's there already, and experiments showed that the difference in run-time was negligible.

- ok, but what about memory? probably negligible because most words will already be repeated for several prefixes?

#### 3.3 Building the Data Structure in C

#### 3.4 Generating Output

> As a rule, try to handle irregularities and exceptions and special cases in data. Code is harder to get right so the control flow should be as simple and regular as possible.

> C is a razor-sharp tool, with which one can create an elegant and efficient program or a bloody mess.

- also the mentioned blood very likely is from the foot the programmer shot himself in ;)

##### Exercise 3-1
The algorithm for selecting a random item from a list of unknown length depends on having a good random number generator. Design and carry out experiments to determine how well the method works in practice.

##### Exercise 3-2
If each input word is stored in a second hash table, the text is only stored once, which should save space. Measure some documents to estimate how much. This organization would allow us to compare pointers rather than strings in the hash chains for prefixes, which should run faster. Implement this version and measure the change in speed and memory consumption.

#### Exercise 3-3
Remove the statements that place sentinel NONWORDs at the beginning and end of the data, and modify generate so it starts and stops properly without them. Make sure it produces correct output for input with 0, 1, 2, 3, and 4 words. Compare this implementation to the version using sentinels.

#### 3.5 Java

- pretty outdated, but still equal performance to modernized approach

##### Exercise 3-4
Revise the Java version of markov to use an array instead of a Vector for the prefix in the State class.

#### 3.6 C++

##### Exercise 3-5
The great strength of the STL is the ease with which one can experiment with different data structures. Modify the C++ version of Markov to use various structures to represent the prefix, suffix list, and state table. How does performance change for the different structures?

##### Exercise 3-6
Write a C++ version that uses only classes and the string data type but no other advanced library facilities. Compare it in style and speed to the STL versions.

#### 3.7 Awk and Perl

- still no idea how awk works

##### Exercise 3-7
Modify the Awk and Perl versions to handle prefixes of any length. Experiment to determine what effect this change has on performance.

#### 3.8 Performance

#### 3.9 Lessons

"The design of a program is rooted in the layout of its data. The data structures don't define every detail, but they do shape the overall solution."

"Less clear, however, is how to assess the loss of control and insight when the pile of system-supplied code gets so big that one no longer knows what's going on underneath."
(they reference the C++ STL, but it applies to almost every framework today...)

"The design and implementation of this program illustrate a number of lessons for larger programs. First is the importance of choosing simple algorithms and data structures, the simplest that will do the job in reasonable time for the expected problem size. If someone else has already written them and put them in a library for you, that's even better [...] Following Brooks's advice, we find it best to start detailed design with data structures, guided by knowledge of what algorithms might be used; with the data structures settled, the code goes together easily."

"It's hard to design a program completely and then build it; constructing real programs involves iteration and experimentation. The act of building forces one to clarify decisions that had previously been glossed over. [...] As much as possible, start with something simple and evolve it as experience dictates."

##### Exercise 3-8
We have seen versions of the Markov program in a wide variety of languages, including Scheme. Tcl, Prolog, Python, Generic Java. ML, and Haskell; eachpresents its own challenges and advantages. Implement the program in your favorite
language and compare its general flavor and performance.


#### Personal summary

- I would have wasted a lot of time thinking about how to tokenize and integrate punctuation, just using the words with and without is such a simple and nice approach!
  There are several simple approaches (e.g. initialize with NONWORD) that would have cost me ages because I'd not think so simple...
- I don't like hardcoding all the parameters, playing with them is more funny when they are easier to change -- but for the sake of simplicity it's ok (and non-interesting busywork compared to design).
- Saving and loading a trained model would be more fun that re-train it every time the program starts.
- Even the C++ and Java work on global state, ok for a small project, but not generally good practice?
- Couldn't make the awk script work.

#### Exercise 3-8 solutions

- Since we have a "solution pattern" it's easier for me to apply that in each language, instead of being more idiomatic in other languages... -.-
  (ok, partly that's the idea, we shouldn't refactore the idea competely, but there are lots of possible structural approaches)
- About the structure of the the Java example: `Prefix` is a state of `Chain`, probably fine from their point of view (modeling the Markov Chain).
  I've adapted that for my Java approach, but another view is to model the "markov model" i.e. a customized map, and that would come more natural to me (e.g. my Typescript approach).
  (May be because of my backgrund, I even use the term "model", not "statetab".)
- In Go I tried both, lazily reimplementing the Perl approach and modeling the Chain.
- Most time wasted in "how to read stdin ... word-by-word?"
  - The "tokenization" is something that is handled differently by different languages, mostly sure each way can be implemented in each language,
    but the "easy" methods are somtimes the input stream can be scanned word-by-word (Go), sometimes we can read newline-determined "lines" and split,
    either by characters (just space) or regexes e.g. \s+, which again works differently in different languages
- In exercises I mostly don't mind generic length prefixes, I use 2...
- Note: If the King James Bible-output doesn't start with "The Project Gutenberg" something is probably wrong...
- King James Bible input starts with what looks like a space, but is actually a BOM, that causes different beginning of output in different languages...
- Generated models have small differences, especially in Typescript.

Embarassing issue with the provided Java code, it compiled fine, but when running:
```
Error: A JNI error has occurred, please check your installation and try again
Exception in thread "main" java.lang.UnsupportedClassVersionError: Chain has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0
```
...because my `javac` referenced Java 17 and my `java` referenced Java 8 m(

- Python version (15min)
  - looks almost exactly like the Perl version (no surprise)

- Go version (45min)
  - looks also very similar (but is probably not particularly good Go?)
  - output is always identical unless rand is explicitly seeded
  - the string slice doesn't ever have to be constructed?

- Go version 2 (30min)
  - probably better structure/more idiomatic Go?
  - re-using type `Prefix` as map key just worked?

- Java version (1+h)
  - surprisingly my slightly modern Java implementation is eqally fast as the provided 20+years old one
  - also surprisingly complicated to get right, although I tried to even simplify on their proposed solution

- Typescript version (2 afternoons)
  - reading from stdin was surprisingly complicated to figure (and ofc a basic course in a web interpreter doesn't cover this)
  - ES6 Map doesn't work with arbitrary types as key, and there doesn't seem to be a mechanic like in java where overriding equals()+hashCode() does the trick, or implementing some interface or adding some trait, or...
  - why does the output start with a newline?

```
$ $(npm bin)/tsc --noImplicitReturns --noFallthroughCasesInSwitch --strict --noUnusedLocals markov.ts
$ node markov.js <The\ King\ James\ Bible.txt | fmt
# or run via REPL but without the extra compiler checks
$ $(npm bin)/ts-node markov.ts <The\ King\ James\ Bible.txt | head
```

Typescript toolchain setup:
```
# install node, via "snap install node --classic" or something...
$ mkdir project && cd project
$ npm init
$ npm install typescript
$ npm install ts-node
# if required
$ npm install -D tslib @types/node[@<node version>]`
# check if it works
$ node --version
$ $(npm bin)/ts-node
```

Provided implementations
```
$ clang -c -o eprintf_clang.o eprintf.c
$ clang -o markovc markov.c eprintf_clang.o 
$ time ./markovc <../kingjamesbible.txt >/dev/null

real	0m1,770s
user	0m1,753s
sys	0m0,016s

$ c++ -Wall -o markovcpp markov++.c
$ time ./markovcpp <../kingjamesbible.txt >/dev/null

real	0m3,789s
user	0m3,735s
sys	0m0,052s

$ javac Markov.java
$ time java Markov <../kingjamesbible.txt >/dev/null 

real	0m0,439s
user	0m1,157s
sys	0m0,082s

$ time perl markov.pl <../kingjamesbible.txt >/dev/null

real	0m0,539s
user	0m0,507s
sys	0m0,032s
```

- the C++ version probably suffers from a bad map implementation
- the C implementation is surprisingly slow -- caused by the fixed state table size (NHASH), all other implementations have resizing containers

C implementation with increased NHASH = 193939
```
$ time ./markov-mod <../kingjamesbible.txt >/dev/null

real	0m0,297s
user	0m0,274s
sys	0m0,024s
```

Personal implementations
```
$ time ./markov.py <kingjamesbible.txt >/dev/null 

real	0m0,942s
user	0m0,892s
sys	0m0,050s

$ javac Markov.java
$ time java Markov <kingjamesbible.txt >/dev/null 

real	0m0,407s
user	0m1,119s
sys	0m0,113s

$ go build -o markovgo markov.go
$ time ./markovgo <kingjamesbible.txt >/dev/null 

real	0m0,334s
user	0m0,513s
sys	0m0,069s

$ go build -o markovgo2 markov2.go
$ time ./markovgo2 <kingjamesbible.txt >/dev/null 

real	0m0,288s
user	0m0,506s
sys	0m0,075s

$ time $(npm bin)/ts-node markov.ts <kingjamesbible.txt >/dev/null

real    0m1,638s
user    0m2,720s
sys     0m0,187s

$ time node markov.js <kingjamesbible.txt >/dev/null 

real	0m0,675s
user	0m0,792s
sys	0m0,092s
```

- Note: `rand.nextInt() % s.size()` is biased towards smaller numbers depending on size of `s`, better use the function that gives an int in a range if available
- `time` can also measures CPU resources (on some systems it must be explicitly installed and called: `apt get install time`, `/usr/bin/time -v`)


### Ch 4 - Interfaces

- the he first sections describe a C interface, obviously the specific choiced would be very different in most other languages, but some of the general advice is still valid
- there's a lot of focus on implementation rather than interfaces up to and including 4.4

mentioned are
* interfaces
* information hiding
* resource management
* error handling
so this seems about more than just interfaces?

- would have expected a bit more focus on parameters, return values, ...

- TODO: check the interfaces of several CSV implementations, Go, Python, Java...

#### 4.1 Comma-Separated Values

> Retrieving numbers by interacting with a web browser is effective but time consuming. It's a nuisance to invoke a browser, wait, watch a barrage of advertisements [...] Conspicuous by its absence in this process is the principle of letting the machine do the work. Browsers let your computer access data on a remote server, but it would be more convenient to retrieve the data without forced interaction.

- ...followed by a 5 line tcl script that can directly download the desired csv data. At some point this whole internet thing drifted in a wrong direction...
- also "watch a barrage of advertisements" -- in 1999 :)

> we do not know of an existing public library to handle CSV, so we will write one ourselves

- no csv libs in 1999?
  * ok, there's no actual "standard", but wtf, the RFC that comes next to one [RFC 4180](https://www.rfc-editor.org/rfc/rfc4180.html) is from 2005?


#### 4.2 A Prototype Library

> We are unlikely to get the design of a library or interface right on the first attempt.

> strtok [...] This is a poor interface.

- was my first idea while reading how it works

> We designed our prototype after examining one data source, and we tested it originally only on data from that same source. Thus we shouldn't be surprised when the first encounter with a different source reveals gross failings. Long input lines, many fields, and unexpected or missing separators all cause trouble. This fragile prototype might serve for personal use or to demonstrate the feasibility of an approach, but no more than that. It's time to rethink the design before we try another implementation. [...] If that program is used by others, the quick choices made in the original design may spell trouble that surfaces years later. This scenario is representative of the history of many bad interfaces. It is a sad fact that a lot of quick and dirty code ends up in widely-used software, where it remains dirty and often not as quick as it should have been anyway.

- most of the issues they noted are not actual interface issues, maybe their way of going by one example isn't the best approach here?


#### 4.3 A Library for Others

> To create an interface that others can use, we must consider the issues listed at the beginning of this chapter: interfaces, information hiding, resource management, and error handling. The interplay among these strongly affects the design. Our separation of these issues is a bit arbitrary, since they are interrelated.

##### interfaces

> What function value should `csvgetline` return? It is desirable to return as much useful information as convenient, which suggests returning the number of fields, as in the prototype. But then the number of fields must be computed even if the fields aren't being used. [...] After several experiments, we decided that `csvgetline` will return a pointer to the original line of input, or NULL if end of file has been reached.

- NULL at EOF makes sense, so one can iterate over the file
- is counting the number of fields while parsing the line so hard? (no, but see information hiding)

> What if the user asks for a non-existent field [...]?
- returning NULL makes sense, "distinguish empty from non-existent" is the key requirement here

##### information hiding

- they made `csvnfield` (counts fields) an extra function
  "If the user calls only `csvgetline`, there's no need to split into fields; lines can be split on demand."
- splitting/counting could be done at once or lazy (and they even mention various levels of lazyness)
- the discuss memory management here, although I'd locate it in the next section on resource management

##### resource management

- user must open/close the file, makes sense

> Does `csvgetline` return the original data or make a copy? We decided that the return value of `csvgetline` is a pointer to the original input, which will be overwritten when the next line is read. Fields will be built in a copy of the input line, and `csvfield` will return a pointer to the field within the copy. With this arrangement, the user must make another copy if a particular line or field is to be saved or changed, and it is the user's responsibility to release that storage when it is no longer needed.

- why have one function reference the original input and the other a copy?
  probably the answer is in the implementation so that leaks implementation details

##### error handling

- they decided to basically do none
- just some advice:

> As a principle, library routines should not just die when an error occurs; error status should be returned to the caller for appropriate action. Nor should they print messages or pop up dialog boxes, since they may be running in an environment where a message would interfere with something else. Error handling is a topic worth a separate discussion of its own, later in this chapter.

##### Specification

- advice and empirical observation

> In a large project, the specification precedes the implementation, because specifiers and implementers are usually different people and may be in different organizations.

> In practice, however, work often proceeds in parallel, with specification and code evolving together, although sometimes the "specification" is written only after the fact to describe approximately what the code does.

> The best approach is to write the specification early and revise it as we learn from the ongoing implementation.

> Even for personal programs, it is valuable to prepare a reasonably thorough specification because it encourages consideration of alternatives and records the choices made.

- often ignored
  - story and task descriptions are too vague and have a different purpose and point of view
  - comments and documentation in code is too detailed and focused on the subject commented on

> This specification still leaves open questions. [...] Nailing down all such puzzles is difficult even for a tiny system, and very challenging for a large one, though it is important to try. One often doesn't discover oversights and omissions until implementation is underway.


- what exactly is the meaning of `static`?
- a huge part of the C implementation just deals with memory management

##### Exercise 4-1
There are several degrees of laziness for field-splitting; among the possibilities are to split all at once but only when some field is requested, to split only thefield requested, or to split up to the field requested. Enumerate possibilities, assess their potential difficulty and benefits, then write them and measure their speeds.

##### Exercise 4-2
Add a facility so separators can be changed (a) to an arbitrary class of characters; (b) to different separators for different fields; (c) to a regular expression (see Chapter 9). What should the interface look like?

##### Exercise 4-3
We chose to use the static initialization provided by C as the basis of a one-time switch: if a pointer is NULL on entry, initialization is performed. Another possibility is to require the user to call an explicit initialization function, which could include suggested initial sizes for arrays. Implement a version that combines the best of both. What is the role of `reset` in your implementation?

##### Exercise 4-4
Design and implement a library for creating CSV-formatted data. The simplest version might take an array of strings and print them with quotes and commas. A more sophisticated version might use a format string analogous to `printf`. Look at Chapter 9 for some suggestions on notation.

#### 4.4 A C++ Implementation

##### Exercise 4-5
Enhance the C++ implementation to overload subscripting with operator [] so that fields can be accessed as csv[i].

##### Exercise 4-6
Write a Java version of the CSV library, then compare the three implementations for clarity, robustness, and speed.

##### Exercise 4-7
Repackage the C++ version of the CSV code as an STL iterator.

##### Exercise 4-8
The C++ version permits multiple independent `Csv` instances to operate concurrently without interfering, a benefit of encapsulating all the state in an object that can be instantiated multiple times. Modify the C version to achieve the same effect by replacing the global data structures with structures that are allocated and initialized by an explicit `csvnew` function.


#### 4.5 Interface Principles

> To prosper, an interface must be well suited for it's task -- simple, general, regular, predictable, robust -- and it must adapt gracefully as its users and its implementation change. Good interfaces follow a set of principles. These are not independent or even consistent, but they help us describe what happens across the boundary between two pieces of software.

- Hide implementation details ("information hiding, encapsulation, abstraction, modularization")
  * Avoid global variables; wherever possible it is better to pass references to all data through function arguments.
  * We strongly recommend against publicly visible data in all forms; it is too hard to maintain consistency of values if users can change variables at will.

- Choose a small orthogonal set of primitives
  - "APIs are sometimes so huge that no mortal can be expected to master them."
  - C-specific: WTF is the difference between `putc` and `fputc`?
    (answer: `putc` may be implemented as macro and hav unwanted side-effects https://stackoverflow.com/questions/14008907/fputc-vs-putc-in-c)
  - `putc/fputc/fprintf/fwrite` -- the "f" seems to be short for file/formatted/function depending on context? 
  - don't add to an interface just because it's possible to do so
  - don't fix the interface when it's the implementation that's broken

- Don't reach behind the user's back
  - "A library function should not write secret files and variables or change global data, and it should be circumspect about modifying data in its caller."
  - "The use of one interface should not demand another one"

- Do the same thing the same way everywhere. Consistency and regularity are important.

> No matter what, there is a limit to how well we can do in designing an interface. Even the best interfaces of today may eventually become the problems of tomorrow, but good design can push tomorrow off a while longer.

#### 4.6 Resource Management

- memory, open files, variables (state)
- "Free a resource in the same layer that allocated it"
- "allocation state of a resource should not change across the interface"
- sounds like RAII
- "The existence of automatic garbage collection does not mean that there are no memory-management issues in a design."
- more complicated with multithreading
  - "reentrant" code, meaning "it works regardless of the number of simultaneous executions"
  - "avoid global variables, static local variables, and any other variable that could be modified while another thread is using it"
  - "key to good multi-thread design is to separate the components so they share nothing except through well-defined interfaces"
  - side effect free functions


#### 4.7 Abort, Retry, Fail?

- "Detect errors at a low level, handle them at a high level"
  - "Appropriate return values are not always obvious"
- "Use exceptions only for exceptional situations"
- "In general, aim to keep the library usable after an error has occurred"

- "don't crash" shouldn't be a general principle, if the library is used wrongly, it's probably best to crash


#### 4.8 User Interfaces



### Ch 5 Debugging

> Why should software be so hard?


> Techniques that help reduce debugging time include
- good design,
- good style,
- boundary condition tests,
- assertions and 
- sanity checks in the code,
- defensive programming,
- well-designed interfaces,
- limited global data, and 
- checking tools.

An ounce of prevention really is worth a pound of cure.


> enable all compiler checks and heed the warnings


#### 5.1 Debuggers


#### 5.2 Good Clues, Easy Bugs

> Debugging involves backwards reasoning

- Look for familiar patterns
- Examine the most recent change
- Don't make the same mistake twice

> Easy code can have bugs if its familiarity causes us to let down our guard. Even when code is so simple you could write it in your sleep, don't fall asleep while writing it.

- Debug it now, not later
- Get a stack trace
- Read before typing

> Take a break for a while; sometimes what you see in the source code is what you meant rather than what you wrote [...] Resist the urge to start typing; thinking is a worthwhile alternative.

- Explain your code to someone else

> One university computer center kept a teddy bear near the help desk. Students with mysterious bugs were required to explain them to the bear before they could speak to a human counselor.


#### 5.3 No Clues, Hard Bugs

- Make the bug reproducible
- Divide and conquer
- Study the numerology of failures (i.e. look for patterns in the erroneous program output)
- Display output to localize your search
- Write self-checking code (and if not to expensive leave checks enabled, or disable but not delete them after fixing so the can be reactivated next time)
- Write a logfile
  - make sure to flush, `stdout` usually is buffered (`stderr` usually isn't)
- Draw a picture
- Use tools
- Keep records
- use tools like `grep`, `diff`, small shell scripts


#### 5.4 Last Resorts

- "mental model bugs"

> Notice, by the way, that in the list example the error was in the test code, which made the bug that much harder to find. It is frustratingly easy to waste time chasing bugs that aren't there, because the test program is wrong, or by testing the wrong version of the program, or by failing to update or recompile before testing.

> Once in a long while, the problem really is the compiler or a library or the operating system or even the hardware


#### 5.5 Non-reproducible Bugs

- Check whether all variables have been initialized
- If the bug changes behavior or even disappears when debugging code is added, it may be a memory allocation error

> If the crash site seems far away from anything that could be wrong, the most likely problem is overwriting memory by storing into a memory location that isn't used until much later. Sometimes this is a dangling pointer problem, where a pointer to a local variable is inadvertently returned from a function, then used. Returning the address of a local variable is a recipe for delayed disaster

- Using a dynamically allocated value after it has been freed has similar symptoms
- In some implementations of malloc and free, freeing an item twice corrupts the internal data structures but doesn't cause trouble until much later, when a subsequent call slips on the mess made earlier

> When a program works for one person but fails for another, something must depend on the external environment of the program. This might include
- files read by the program,
- file permissions,
- environment variables,
- search path for commands,
- defaults, or 
- startup files.

It's hard to be a consultant for these situations, since you have to become the other person to duplicate the environment of the broken program.


##### Exercise 5-1.
Write a version of malloc and free that can be used for debugging storage-management problems. One approach is to check the entire workspace on each call of malloc and free; another is to write logging information that can be processed by another program. Either way, add markers to the beginning and end of each allocated block to detect overruns at either end.


#### 5.6 Debugging Tools

(other than debuggers)

- `strings`


##### Exercise 5-2
The `strings` program prints strings with MINLEN or more characters, which sometimes produces more output than is useful. Provide `strings` with an optional argument to define the minimum string length.

##### Exercise 5-3
Write `vis`, which copies input to output, except that it displays non-printable bytes like backspaces, control characters, and non-ASCII characters as `\Xhh` where hh is the hexadecimal representation of the non-printable byte. By contrast with `strings` , `vis` is most useful for examining inputs that contain only a few non-printing characters.

##### Exercise 5-4
What does `vis` produce if the itput is `\XOA`? How could you make the output of `vis` unambiguous?

##### Exercise 5-5
Extend `vis` to process a sequence of files, fold long lines at any desired column, and remove non-printable characters entirely. What other features might be consistent with the role of the program?


#### 5.7 Other People's Bugs

- "discovery" -- discovering what on earth is going on in something that you didn't write (how the program is organized and how the original programmers thought and wrote)
- If you think that you have found a bug in someone else's program, the first step is to make absolutely sure it is a genuine bug, so you don't waste the author's time and lose your own credibility

Bug reporting
- provide [...] as good a test case as you can manage
- strip the test down to a minimal and self-contained case
- include other information that could possibly be relevant, like the version of the program itself,  and of the compiler, operating system, and hardware
- send the kind of bug report you'd like to receive yourself


#### 5.8 Summary


#### Discussion

- underwhelming chapter, nothing wrong, but very light
- missed (or just slightly touched) topic: writing your own tools to analyze or explore the problem space
- `strings` has a UNIX equivalent (same name)
- recommended UNIX tool: `xxd` (better `hexdump`)
- ex 5.1: it's common to already have this in managed/modern languages, e.g. there's a Go tool `pprof` that basically does this
- recommended book: "Code Complete"



### Ch 6 Testing

#### 6.1 Test as You Write the Code

- Test code at its boundaries
- Test pre- and post-conditions
- Use assertions
- Program defensively
- Check error returns



##### Exercise 6-1
Check out these examples at their boundaries, then fix them as necessary according to the principles of style in Chapter I and the advice in this chapter.

(a) This is supposed to compute factorials:
```
int factoria1 (int n)
{
    int fac;
    fac = 1;
    while (n--1)
        fac a = n;
    return fac;
}
```

(b) This is supposed to print the characters of a string one per line:
```
i=O;
do {
    putchar(s[i++]);
    putchar('\n');
) while (s[i] != '\0');
```

(c) This is meant to copy a string from source to destination:
```
void strcpy(char *dest, char *src)
{
    int i;
    for (i = 0; src[i] != '\0'; i++)
        dest[i] = src[i]
}
```

(d) Another string copy, which attempts to copy n characters from s to t:
```
void strncpy(char *t, char *s, int n)
{
    while (n > 0 && *s != '\O') {
        *t = *s;
        t++;
        s++;
        n--;
    }
}
```

(e) A numerical comparison:
```
if (i > j)
    printf ("%d is greater than %d.\n", i, j);
else
    printf ("%d is smaller than %d.\n", i, j);
```

(f) A character class test:
```
if (c >= 'A' && c <= 'Z') {
    if (c <= 'L')
        cout << "first half of alphabet";
    else
        cout << "second half of alphabet";
}
```

##### Exercise 6-2
As we are writing this book in late 1998, the Year 2000 problem looms as perhaps the biggest boundary condition problem ever.

(a) What dates would you use to check whether a system is likely to work in the year 2000? Supposing that tests are expensive to perform. in what order would you do your tests after trying January 1, 2000 itself?

(b) How would you test the standard function `ctime`, which returns a string representation of the date in this form:
        Fri Dec 31 23:58:27 EST 1999\n\0
Suppose your program calls `ctime`. How would you write your code to defend against a flawed implementation?

(c) Describe how you would test a calendar program that prints output like this:
```
       January 2000
     S  M Tu  W Th  F  S
                       1
     2  3  4  5  6  7  8
     9 10 11 12 13 14 15
    16 17 18 19 20 21 22
    23 24 25 26 27 28 29
    30 31 
```

(d) What other time boundaries can you think of in systems that you use, and how would you test to see whether they are handled correctly?


#### 6.2 Systematic Testing

- Test incrementally
- Test simple parts first
- Know what output to expect
- Verify conservation properties
- Compare independent implementations
- Measure test coverage

##### Exercise 6-3
Describe how you would test `freq`.

##### Exercise 6-4
Design and implement a version of `freq` that measures the frequencies of other types of data values, such as 32-bit integers or floating-point numbers. Can you make one version of the program handle a variety of types elegantly?


#### 6.3 Test Automation

- Automate regression testing
- Create self-contained tests

##### Exercise 6-5
Design a test suite for `printf`, using as many mechanical aids as possible.


#### 6.4 Test Scaffolds

##### Exercise 6-6
Create the test scaffold for memset along the lines that we indicated.

##### Exercise 6-7
Create tests for the rest of the `mem...` family.

##### Exercise 6-8
Specify a testing regime for numerical routines like `sqrt`, `sin`, and so on, as found in `math.h`. What input values make sense? What independent checks can be performed?

##### Exercise 6-9
Define mechanisms for testing the functions of the `C str...` family, like `strcmp`. Some of these functions, especially tokenizers like `strtok` and `strcspn`, are significantly more complicated than the `mem...` family, so more sophisticated tests will be called for.


#### 6.5 Stress Tests

##### Exercise 6-10
Try to create a file that will crash your favorite text editor, compiler, or other program.


#### 6.6 Tips for Testing

#### 6.7 Who Does the Testing?

#### 6.8 Testing the Markov Program

#### 6.9 Summary


#### Discussion


- regression tests (comparing old-version output to new-version output)?
  - "create self-contained tests"
  - standalone program tests
- testing levels: unit, integration, system
  - not mentioned in the chapter, instead regression testing, stress tests, sort of integration and 
  - unit tests are not mentioned but "test scaffolds" partly describe them -- were unit tests or the today typical testing levels/methods not yet a thing in the late 90s?
- bad habits
  - no tests
  - (re)designing code to be "testable"
  - e.g. making methods public that should be private
- love-hate relationship with tests
  - e.g. several flow "integration" tests
  - tests should save time by pointing directly to what went wrong and where, but sometimes waste time because they are too complicated to unterstand what's going on
  - test setup can take longer than the actual implementation
- test driven programming -- doesn't work, programming is too dynamic
- language-specific approaches (ADA, ...)
- automatic verification

---

instead of "testing business hierarchies"
- fully automated vs human interaction
- fast vs slow
- self-contained vs dependent on other systems
- reliable vs flaky

- Joe made some good arguments for high coverage
- arguments against unit tests in favor of higher order/more complex tests (functional, behaviour, ...)
- only value of unit tests is they point out a breaking location faster
- functional is better because functional quality of a program rarely changes

- fuzz testing

- property testing

- mutation testing
  - go mutesting (?)


### Ch 7 Performance

- "the first principle of optimization is don't"
  - only if the problem is important, the program is genuinely too slow, and there is some expectation that it can be made faster while maintaining correctness, robustness, and clarity
  - Is the program good enough already?
  - How will the program be used and what environment it runs in?
  - Is there any benefit to making it faster?

#### 7.1 A Bottleneck

- simple spam filter, refactored to use a different data structure

##### Exercise 7-1
A table that maps a single character to the set of patterns that begin with that character gives an order of magnitude improvement. Implement a version of `isspam` that uses two characters as the index. How much improvement does that lead to? These arc simple special cases of a data structure called a trie. Most such data structures are based on trading space for time.

#### 7.2 Timing and Profiling

- "Automate timing measurements", e.g. with `time`
- "Use a profler"
- "Concentrate on the hot spots"
- "Draw a picture"
- (automated) benchmarks are not mentioned

some discussions found while looking for profilers
- https://stackoverflow.com/questions/1794816/recommendations-for-c-profilers
- https://stackoverflow.com/questions/406760/whats-your-most-controversial-programming-opinion/1562802#1562802
- https://stackoverflow.com/questions/1777556/alternatives-to-gprof/1779343#1779343
- https://stackoverflow.com/questions/375913/how-do-i-profile-c-code-running-on-linux/378024#378024
- https://stackoverflow.com/questions/926266/performance-optimization-strategies-of-last-resort/927773#927773

##### Exercise 7-2
Whether or not your system has a `time` command, use `clock` or `getTime` to write a timing facility for your own use. Compare its times to a wall clock. How does other activity on the machine affect the timings?

##### Exercise 7-3
In the first profile, `strchr` was called 48,350,000 times and `strncmp` only 46,180,000. Explain the difference.

- `strchr` is called unconditionally and `strncmp` is guarded, only called if the `strchr` return value is not NULL

#### 7.3 Strategies for Speed
- "Use a better algorithm or data structure"
- "Enable compiler optimizations"
- "Tune the code"
- "Don't optimize what doesn't matter"

#### 7.4 Tuning the Code
- "Collect common subexpressions"
- "Replace expensive operations by cheap ones"
- "Unroll or eliminate loops"
- "Cache frequently-used values"
- "Write a special-purpose allocator"
- "Buffer input and output"
- "Handle special cases separately"
- "Precompute results"
- "Use approximate values"
- "Rewrite in a lower-level language"

##### Exercise 7-4
One way to make a function like `memset` run faster is to have it write in word-sized chunks instead of byte-sized; this is likely to match the hardware better and might reduce the loop overhead by a factor of four or eight. The downside is that there are now a variety of end effects to deal with if the target is not aligned on a word boundary and if the length is not a multiple of the word size. Write a version of `memset` that does this optimization. Compare its performance to the existing library version and to a straightforward byte-at-a-time loop.

##### Exercise 7-5
Write a memory allocator `smalloc` for C strings that uses a special-purpose allocator for small strings but calls `malloc` directly for large ones. You will need to define a struct to represent the strings in either case. How do you decide where to switch from calling `smalloc` to `malloc`?

#### 7.5 Space Efficiency
- "first approach to optimizing space should be the same as to improving speed: don't bother"
- "Save space by using the smallestpossible data type"
- "Don't store what you can easily recompute"

#### 7.6 Estimation
- they wrote a program to estimate the cost of individual statements, some surprising results:
  - float arithmetic is as fast as integer arithmetic
  - float division is faster than integer division
  - inequality check (!=) is slower than equality check (==)
- better check these results on our own machines

##### Exercise 7-6
Create a set of tests for estimating the costs of basic operations for computers and compilers near you, and investigate similarities and differences in performance.

##### Exercise 7-7
Create a cost model for higher-level operations in C++. Among the features that might be included are construction, copying, and deletion of class objects; member function calls; virtual functions; inline functions; the iostream library; the STL. This exercise is open-ended, so concentrate on a small set of representative operations.

##### Exercise 7-8
Repeat the previous exercise for Java.

#### 7.7 Summary


### Ch 8 Portability

#### 8.1 Language

* Stick to the standard
* Program in the mainstream
* Beware of language trouble spots
  - Size of data types
  - Order of evaluation
In C and C++, the order of evaluation of operands of expressions, side effects, and function arguments is not defined. For example, in the assignment
`n = (getchar() << 8) | getchar();`
the second getchar could be called first: the way the expression is written is not necessarily the way it executes. In the statement
`prt[count] = name[++count];`
count might be incremented before or after it is used to index ptr, and in
`printf("%c %c\n", getchar(), getchar());`
the first input character could be printed second instead of first. In
`printf("%f %f\n", log(-1.23), strerror(errno));`
the value of errno may be evaluated before log is called.
  - Signedness of `char` - In C and C++ it is not specified whether the char data type is signed or unsigned, always store a `getchar()` return in an int (e.g. for comparison with `EOF` which is -1)
  - Arithmetic or logical shift - Right shifts of signed quantities with the >> operator may be arithmetic (a copy of the sign bit is propagated during the shift) or logical (zeros fill the vacated bits during the shift).
  - Byte order
  - Alignment of structure and class members
  - Bitfields - Bitfields are so machine-dependent that no one should use them.
Don't use side effects except for a very few idiomatic constructions like
        a[i++] = 0;
        c = *p++;
        *s++ = *t++;

* Try several compilers

#### 8.2 Headers and libraries

* Use standard libraries

#### 8.3 Program organization

- union vs intersection approach
  - union: use the best features of each particular system, and make the compilation and installation process conditional on properties of the local environment
    "union code is by design unportable"
  - intersection: use only those features that exist in all target systems; don't use a feature if it isn't available everywhere

* Use only features available everywhere
* Avoid conditional compilation (preprocessor `#ifdef`, C-specific)

##### Exercise 8-1
Investigate how your compiler handles code contained within a conditional block like
        const ind DEBUG = 0;
        /* or enum { DEBUG = 0 }; */
        /* or final boolean DEBUG = false; */
        if (DEBUG) {
            ...
        }
Under what circumstances does it check syntax? When does it generate code? If you have access to more than one compiler, how do the results compare?

#### 8.4 Isolation

* Localize system debendencies in separate files
* Hide system dependencies behind interfaces

#### 8.5 Data Exchange

* Use text for data exchange

##### Exercise 8-2
Write a program to remove spurious carriage returns from a file. Write a second program to add them by replacing each newline with a carriage return and newline. How would you test these programs?

#### 8.6 Byte order

* Use a fixed byte order for data exchange

#### 8.7 Portability and Upgrade

* Change the name if you change the specification
  - examples: `echo`, `sum`
* Maintain compatibility with existing programs and data

#### 8.8 Internationalization

* Don't assume ASCII
  - today (2022-11), use UTF-8
* Don't assume english

#### 8.9 Summary


### Ch 9 Notation

#### 9.1 Formatting Data

- generic `printf`-like pack and unpack routines

##### Exercise9-1
Modify `pack` and `unpack` to transmit signed values correctly, even between machines with different sizes for `short` and `long`. How should you modify the format strings to specify a signed data item? How can you test the code to check, for example, that it correctly transfers a -1 from a computer with 32-bit longs to one with 64-bit longs?

##### Exercise 9-2
Extend `pack` and `unpack` to handle strings; one possibility is to include the length of the string in the format string. Extend them to handle repeated items with a count. How does this interact with the encoding of strings?

##### Exercise 9-3
The table of function pointers in the C program above is at the heart of C++'s virtual function mechanism. Rewrite `pack` and `unpack` and `receive` in C++ to take advantage of this notational convenience.

##### Exercise 9-4
Write a command-line version of `printf` that prints its second and subsequent arguments in the format given by its first argument. Some shells already provide this as a built-in.

##### Exercise 9-5
Write a function that implements the format specifications found in spreadsheet programs or in Java's Decimal Format class, which display numbers according to patterns that indicate mandatory and optional digits, location of decimal
points and commas, and so on. To illustrate, the format `##,##0.00` specifies a number with two decimal places, at least one digit to the left of the decimal point, a comma after the thousands digit, and blank-filling up to the ten-thousands place. It would represent 12345.67 as 12,345.67 and .4 as _____0.40 (using underscores to stand for blanks). For a full specification, look at the definition of `DecimalFormat` or a spreadsheet program.

#### 9.2 Regular Expressions

- simple regex matching function, supporting `^ & . and *`

##### Exercise 9-6
How does the performance of `match` compare to `strstr` when search- ing for plain text?

##### Exercise 9-7
Write a non-recursive version of `matchhere` and compare its performance to the recursive version.

##### Exercise 9-8
Add some options to grep. Popular ones include `-v` to invert the sense of the match, `-i` to do case-insensitive matching of alphabetics, and `-n` to include line numbers in the output. How should the line numbers be printed? Should they be printed on the same line as the matching text?

##### Exercise 9-9
Add the + (one or more) and ? (zero or one) operators to match. The pattern a+bb? matches one or more a's followed by one or two b's.

##### Exercise 9-10
The current implementation of `match` turns off the special meaning of ^ and $ if they don't begin or end the expression, and of * if it doesn't immediately follow a literal character or a period. A more conventional design is to quote a metacharacter by preceding it with a backslash. Fix `match` to handle backslashes this way.

##### Exercise 9-11
Add character classes to `match`. Character classes specify a match for any one of the characters in the brackets. They can be made more convenient by adding ranges, for example `[a-z]` to match any lower-case letter, and inverting the sense, for example `[^O-9] to match any character except a digit.

##### Exercise 9-12
Change match to use the leftmost-longest version of matchstar, and modify it to return the character positions of the beginning and end of the matched text. Use that to build a program `gres` that is like grep but prints every input line after substituting new text for text that matches the pattern, as in
        % gres 'homoiousian' ' homoousian' mission. stmt

##### Exercise 9-13
Modify match and grep to work with UTF-8 strings of Unicode characters. Because UTF-8 and Unicode are a superset of ASCII, this change is upwardly compatible. Regular expressions, as well as the searched text, will also need to work properly with UTF-8. How should character classes be implemented?

##### Exercise 9-14
Write an automatic tester for regular expressions that generates test expressions and test strings to search. If you can, use an existing library as a reference implementation; perhaps you will find bugs in it too.


#### 9.3 Programmable Tools

- shell scripts, command interpreter, combining programms
- example with awk, perl, fmt



